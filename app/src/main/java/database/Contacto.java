package database;

import java.io.Serializable;

public class Contacto implements Serializable {
    private int _ID;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private String direccion;
    private String notas;
    private int favorite;
    public Contacto(){
        this._ID = 0;
        this.nombre = "";
        this.telefono1 = "";
        this.telefono2 = "";
        this.direccion = "";
        this.notas = "";
        this.favorite = 0;
    }
    public Contacto(int _ID, String nombre, String telefono1, String telefono2, String direccion, String notas, int favorite) {
        this._ID = _ID;
        this.nombre = nombre;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.direccion = direccion;
        this.notas = notas;
        this.favorite = favorite;
    }
    public int get_ID() {
        return _ID;
    }
    public void set_ID(int _ID){
        this._ID = _ID;
    }
    public String getNombre(){
        return nombre;
    }

}
